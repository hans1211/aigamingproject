# 環境設定
助教建議在Windows10上開發

使用Python3.6或更新（我是用3.7.3）

虛擬環境不用Anaconda，用官方推薦的Pipenv

用原版的Python3.7，裝Pipenv就好

Pipfile已經給你了，環境會自動幫你用好

不會設定就裝Pycharm吧



# Pycharm & Git 聯動

下載Pycharm：<https://www.jetbrains.com/pycharm/>

社群版（免費版）基本夠用，不過用學校的信箱註冊可以免費用他們公司所有產品的專業版（年費649美金，佛心公司）

另外你有在開發Java，JetBrains的招牌就是他們的Java IDE：Intellij IDEA，地表最強，你值得擁有

裝好後打開是外觀設定，你自己搞定。

然後來到開始畫面：

![1557847999266](./assets/1557847999266.png)

點Check out from Version Control | Git

![1557848103283](./assets/1557848103283.png)

貼上這個Project的Git網址：

https://gitlab.com/hans1211/aigamingproject.git

可選本機的儲存位置

這樣就完成Clone了（會要求登入Gitlab）



請確認在原版Python裡有裝Pipenv

從左側的Project標籤打開一個py檔，點Fix Pipenv interpreter就自動幫你設定好環境了

![1557848401609](./assets/1557848401609.png)

你應該在右下角會看到Pipenv

![1557849741776](assets/1557849741776.png)

另外左邊的標籤可以快速切換Branch



## 我的Git哪有這麼簡單

一切都發生在頂端的VCS（Version Control System）選單

![1557849422512](assets/1557849422512.png)

或是用右上角有快速的按鍵可以Update/Commit/ShowHistory

![1557849557415](assets/1557849557415.png)

Commit時會出現畫面：

![1557849886311](assets/1557849886311.png)

右半邊的Before Commit勾選跟我一樣就好



## Push

如果你的Gitlab還沒有你的電腦的SSH公鑰是不能Push上去的

### 產生SSH key

開啟Terminal（Win+R, cmd）

輸入```ssh-keygen```

出現```Enter file in which to save the key```

不用輸入直接Enter

然後根據要求進行，Email要跟Gitlab一樣

通關密碼你自己決定要不要打。

之後在```C:\Users\<yourname>\.ssh```

可以找到```id_ras.pub```

回到Gitlab網站，進入Settings

![1557850770506](assets/1557850770506.png)

右邊標籤選SSH Keys

用文字編輯器吧```id_ras.pub```打開，全部複製上去，Add key

![1557851000334](assets/1557851000334.png)

回到Terminal輸入

```ssh -T git@gitlab.com```

如果正常會看到

```Welcome to GitLab,  @steven112163!```

在Pycharm裡，VCS | Git | Push就能正常運作了

![1557851142266](assets/1557851142266.png)



## Pull

每次開打時記得先Pull下來

在Pycharm裡Push正上方就是了，或者右上角快捷鍵的Update也OK





# 教練我想學Debug

在行碼旁邊點一下，設定中斷點

![1557923936221](assets/1557923936221.png)

在右上角點選Debug或右鍵選單選Debug

![1557924001180](assets/1557924001180.png)

可以在下方的Debugger直接查看資料結構

![1557924087579](assets/1557924087579.png)

![1557924204554](assets/1557924204554.png)

上面的按鍵，你會用到的箭頭是

第一個：Step Over 執行到下一行

第三個：Step into my code 避免進入內建函數

綠色按鍵：Run to Cursor 

這三個就很夠用了

## Python交互式界面

![1557924536777](assets/1557924536777.png)

切換到Console標籤

點py按鈕（找不到就點下面三顆點）

![1557924680657](assets/1557924680657.png)

就可以用Python交互界面協助Debug

