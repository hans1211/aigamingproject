from Compact_version.Team_2 import ChessBoard, GameAgent

if __name__ == '__main__':
    global_state = ChessBoard(new_board=True, game_turn=0,
                              is_black=True, is_black_turn=True,
                              input_board=None)
    global_state.clear_board()
    global_state.put_piece((0, 0), 1)
    global_state.put_piece((0, 4), 1)
    global_state.put_piece((1, 4), 2)
    global_state.print_board()
    for x in range(200):
        player = GameAgent(is_black=global_state.is_black_turn,
                           state=global_state.board,
                           game_turn=x)
        global_state = global_state.move_piece(player.get_step())
        global_state.print_board()
